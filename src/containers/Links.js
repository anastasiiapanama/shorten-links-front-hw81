import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {changeHandlerValue, createLink, fetchOneLink} from "../store/action";

import Grid from "@material-ui/core/Grid";
import {Button, Link, TextField, Typography} from "@material-ui/core";

const Links = () => {
    const dispatch = useDispatch();
    const originalUrl = useSelector(state => state.originalUrl);
    const short = useSelector(state => state.short);

    const changeHandler = event => {
        dispatch(changeHandlerValue(event.target));
    };

    const handlerShorten = () => {
        dispatch(createLink({originalUrl}));
    };

    const handleRedirect = () => {
        dispatch(fetchOneLink(short));
    };

    return (
        <Grid container spacing={0} direction="column" alignItems="center" justify="center" style={{minHeight: '100vh'}}>
            <Grid item direction="column" style={{paddingBottom: '20px'}}>
                <Typography variant="h3">Shorten your link</Typography>
            </Grid>
            <Grid item direction="column" style={{width: '50%'}}>
                <TextField
                    variant="outlined"
                    fullWidth
                    type="content"
                    name="originalUrl"
                    value={originalUrl}
                    required
                    onChange={e => changeHandler(e)}
                    multiline
                    rows={0}
                />
            </Grid>
            <Grid item direction="column">
                <Button type="submit" style={{backgroundColor: '#648dae', marginTop: '20px'}}
                        variant="contained"
                        onClick={handlerShorten}
                >
                    Shorten!
                </Button>
            </Grid>
            <Grid item direction="column">
                <Typography variant='h5' style={{paddingTop: '20px'}}>Your link now looks like this:</Typography>
            </Grid>
            <Grid item direction='column'>
                <Link style={{cursor: 'pointer', paddingTop: '30px', fontSize: '25px'}} onClick={handleRedirect}>{short}</Link>
            </Grid>
        </Grid>
    );
};

export default Links;