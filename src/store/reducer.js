import {CHANGE_HANDLER_VALUE, CREATE_LINK_SUCCESS} from "./action";

const initialState = {
    originalUrl: '',
    short: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_HANDLER_VALUE:
            return {...state, [action.event.name]: action.event.value};
        case CREATE_LINK_SUCCESS:
            return {...state, short: action.link};
        default:
            return state;
    }
};

export default reducer;