import axiosApi from "../axios-api";

export const CREATE_LINK_SUCCESS = 'CREATE_LINK_SUCCESS';

export const CHANGE_HANDLER_VALUE = 'CHANGE_HANDLER_VALUE';

export const createLinkSuccess = link => ({type: CREATE_LINK_SUCCESS, link});

export const changeHandlerValue = event => ({type: CHANGE_HANDLER_VALUE, event});

export const createLink = linkData => {
    return async dispatch => {
        try {
            const response = await axiosApi.post('/links', linkData);

            dispatch(createLinkSuccess(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const fetchOneLink = shortUrl => {
    return async dispatch => {
        try {
            await axiosApi.get('/links/' + shortUrl);
        } catch (e) {
            console.log(e);
        }
    };
};